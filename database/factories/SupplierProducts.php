<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Products;
use Faker\Generator as Faker;

$factory->define(Products::class, function (Faker $faker) {
    return [
        'Product_Name' => $faker->Product_Name,
        'Mota' => $faker->Mota,
        'created_at' => new DateTime,
        'updated_at' => new DateTime,
        'id_groups' => $faker->id_groups,
    ];
});
