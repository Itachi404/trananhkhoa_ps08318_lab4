<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProduct extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->Increments('id_products');
            $table->string('Product_Name',75);
            $table->text('Mota')->nullable();
            $table->timestamps();
        });
        Schema::table('products', function ($table) {
            $table->bigInteger('id_groups')->unsigned()->index();
            $table->foreign('id_groups')
                  ->references('id_groups')
                  ->on('groups')
                  ->onDelete('cascade')
                  ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
