<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        //$this->call(SupplierTableSeeder::class);
        DB::table('groups')->insert([
            ['Group_Name' => 'Dụng cụ','created_at' => new DateTime,'updated_at' => new DateTime],
            ['Group_Name' => 'Tiện ích','created_at' => new DateTime,'updated_at' => new DateTime],
        ]);
        DB::table('products')->insert([
            ['Product_Name' => 'Súng','Mota' => 'Bắn rất sướng','id_groups' => '1','created_at' => new DateTime,'updated_at' => new DateTime],
            ['Product_Name' => 'Katana','Mota' => 'Chém rất gọt','id_groups' => '2','created_at' => new DateTime,'updated_at' => new DateTime],
        ]);
    }
}
